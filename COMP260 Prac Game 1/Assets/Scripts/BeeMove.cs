﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    public float speed = 12.0f; // metres per second
    public float turnSpeed = 180.0f; // degrees per second
    public Transform target;
    public Transform PlayerI;
    public Transform PlayerII;
    private Vector2 heading = Vector2.right;
    private Vector2 relativeDistance;


    void Update()
    {
        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        // get the vector from the bee to Players
        Vector2 direction2 = PlayerI.position - transform.position;
        Vector2 direction3 = PlayerII.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);

        if (direction2.magnitude < direction3.magnitude)
        {
            target = PlayerI;
        }
        else
        {
            target = PlayerII;
        }
    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

}
